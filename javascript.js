/*
Fuer aboutus.html
 */
function onloadCreator() {
    createDiv()
    printBrowserType()
    printLastVisitDate()
    init();
}

function createDiv() {
    // Create element; can be whatever you want, e. g. div, h1, p, img...
    const div = document.createElement("div");
    div.id = "divBrowserType"
    const cDiv = document.createElement("div");
    cDiv.id = "cDiv"

    // Set some attributes
    div.style.width = '200px';
    div.style.height = '200px';

    // Append the div to the body
    document.body.appendChild(div);
    document.body.appendChild(cDiv);
}

function printBrowserType() { // Ausgabe des Browsertyps
    document.getElementById("divBrowserType").innerHTML = "navigator.product is " + navigator.product;
}

function printLastVisitDate() { //Ausgabe des Letzten Besuches mithilfe eines Cookies
    document.getElementById("cDiv").innerHTML = "Your last visit was: " + getCookie("lastVisit");
}

function getCookie(cname) {  //Abgespeichert Cookie wird rangeholt
    const name = cname + "=";
    const decodedCookie = decodeURIComponent(document.cookie);
    const ca = decodedCookie.split(';');
    for(let i = 0; i <ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) === ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) === 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function setCookie(cname, cvalue, exdays) {  //Cookie wird gesetzt und initzialisiert
    const d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    const expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires;
}

window.onbeforeunload = function(){
    setCookie("lastVisit", Date(), 365) // Cookie wird mit neusten Daten beschrieben
}

function init() {
    window.requestAnimationFrame(draw); // Sagt dem Browser, dass eine Animation angefordert wird (Eingrenzung)
}

function draw() { // Zeichnen der Animation
    const ctx = document.getElementById('myCanvas').getContext('2d');

    ctx.globalCompositeOperation = 'destination-over';
    ctx.clearRect(0, 0, 500, 500); // clear canvas
    ctx.save();
    ctx.translate(250, 250);

    const time = new Date(); // inplementierung des Timers
    ctx.rotate(((2 * Math.PI) / 30) * time.getSeconds() + ((2 * Math.PI) / 30000) * time.getMilliseconds());
    ctx.translate(105, 0);
    ctx.fillRect(0, 0, 40, 24); // Shadow
    ctx.restore();
    ctx.beginPath();
    ctx.stroke();

    window.requestAnimationFrame(draw); // Ende der Eingrenzung
}




/*
Fuer personality.html
 */

function xmlHttpPost(strURL) { // Daten der AJAX-Request werden empfangen
    console.log("xmlHttPost() initiated!") // Debug Nachricht
    let xmlHttpReq;
    // Mozilla/Safari
    if(window.XMLHttpRequest) {
        xmlHttpReq = new XMLHttpRequest();
    }
    // IE
    else if(window.ActiveXObject) {
        xmlHttpReq = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlHttpReq.open('POST', strURL, true);
    xmlHttpReq.timeout = 0;
    xmlHttpReq.setRequestHeader('Content-Type', 'application/JSON');
    xmlHttpReq.onreadystatechange = function() {
        if (xmlHttpReq.readyState === 4) {
            updatepage(xmlHttpReq.responseText);
        }
    }

    const form = document.forms['personalityForm'];
    const values = new FormData(form);
    const query = Object.fromEntries(values.entries());

    xmlHttpReq.send(JSON.stringify(query)); // Verarabeitete Daten werden an den Server geschickt
}
    function updatepage(str){
    document.getElementById("result").innerHTML= "Result:" + "<br/>" + str; // Angegebene Formulardaten werden als Antwort ausgegeben
}

function fillForm() { // Bei betätigen des Buttons wird das Formular mit Daten gefüllt.
    console.log("fillForm initiated!")
    let genderButton = document.getElementById("gender_m");
    genderButton.checked = true;
    document.getElementById('date').value = '2021-01-21';
    document.getElementById('email').value = 'test@test.de';
    document.getElementById('pwd').value = 'yeet1337';
    document.getElementById('fullname').value = 'V';
    document.getElementById('userDistrict').value = 'City Center';
    document.getElementById('uFood').value = 'Chromanticore';
    document.getElementById('uCarBrand').value = 'Quadra';
    document.getElementById('uActivity').value = 'Shootin n lootin';
    document.getElementById('corp').value = 'Militech';


}